#!/bin/sh
# source https://gist.github.com/DarrenN/8c6a5b969481725a4413
grep -m1 version package.json | awk -F: '{ print $2 }' | sed 's/[", ]//g'
