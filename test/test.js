'use strict'

const { describe, before, beforeEach, afterEach, it } = require('mocha')
const series = require('async/series')
const expect = require('chai').expect
require('../src/server')
const net = require('net')
const zmq = require('zeromq/v5-compat')
const utils = require('../src/utils')
const Redis = require('ioredis')

describe('tcp', function () {
  this.timeout(60000)
  const hostTcp = 'localhost'
  const portTcp = 19969
  let _client = new Redis(utils.getRedisUri())
  const client = new net.Socket()
  const push = zmq.socket('push')
  const raw = Buffer.from(
    '<|1|0|2020-05-09T17:34:00-04:00|-71.98765|-33.56789|>'
  )

  before(done => {
    push.bind('tcp://0.0.0.0:3335', err => {
      if (err) throw err
      done()
    })
  })

  describe('normal', function () {
    beforeEach(() => _client.flushall())

    it('should receive and send data to checker', done => {
      const pull = zmq.socket('pull')
      pull.bind('tcp://0.0.0.0:3334')
      series(
        [
          cb => client.connect(portTcp, hostTcp, cb),
          cb => setTimeout(() => client.write(raw, cb), 5000),
          cb => {
            client.end()
            cb()
          }
        ],
        err => {
          if (err) throw err
        }
      )
      pull.on('message', (type, raw) => {
        raw = typeof raw !== 'undefined' ? raw.toString() : null
        if (raw === null) done()
        const data = JSON.parse(raw)
        expect(type).to.eql(Buffer.from('data'))
        expect(data.userId).to.eql(1)
        expect(data.userType).to.eql(0)
        expect(data.datetime).to.eql('2020-05-09T21:34:00.000Z')
        done()
      })
    })

    afterEach(() => _client.flushall())
  })
})
