# tcp

[![build status](https://gitlab.com/covid-1969/tcp/badges/master/build.svg)](https://gitlab.com/covid-1969/tcp/commits/master)
[![coverage report](https://gitlab.com/covid-1969/tcp/badges/master/coverage.svg)](https://gitlab.com/covid-1969/tcp/commits/master)

TCP Server for receive position data from infected people on real time, check if the position is where it's has to be and notify if it's not.
