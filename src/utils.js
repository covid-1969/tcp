'use strict'

const moment = require('moment-timezone')
const geohash = require('ngeohash')

/*
  Returns ZeroMQ Push URI
*/
const getPushUri = () => {
  let uri = 'tcp://127.0.0.1:3334'
  if (process.env.ZRM_CHECKER_PUSH_URL) {
    uri = process.env.ZRM_CHECKER_PUSH_URL
  }
  return uri
}

/*
  Returns Redis url
*/
const getRedisUri = () => {
  let uri = 'redis://localhost:6379'
  if (process.env.REDIS_URI) {
    uri = process.env.REDIS_URI
  }
  return uri
}

/*
  Returns Redis IP
*/
const getRedisIP = () => {
  let ip = 'localhost'
  if (process.env.REDIS_IP) {
    ip = process.env.REDIS_IP
  }
  return ip
}

/*
  Returns Redis URI
*/
const getRedisPORT = () => {
  let port = 6379
  if (process.env.REDIS_PORT) {
    port = process.env.REDIS_PORT
  }
  return port
}

/*
  Returns Redis password from ENV
*/
const getRedisPassword = () => {
  return process.env.PASSWORD || 'password'
}

/*
  Returns a geohash for the point
*/
const getGeohash = (lat, lng) => {
  if (lat && lng) {
    return geohash.encode(lat, lng, 9)
  }
  return ''
}

/*
  Parses data from RAW
  <|userId|userType|datetime|latitude|longitud|>
  Ex: <|1|0|2020-05-09T17:34:00-04:00|-32.972140|-71.543212|>
*/
const parse = data => {
  const d = data.split('|')
  if (d.lenght < 7) return null
  if (!d[5] || !d[4]) return null
  return {
    userId: d[1] !== '' ? parseInt(d[1], 10) : null,
    userType: d[2] !== '' ? parseInt(d[2], 10) : null,
    datetime: d[3] ? moment(d[3]).toISOString() : null,
    point: {
      type: 'Point',
      coordinates: [parseFloat(d[5]), parseFloat(d[4])]
    },
    geohash: getGeohash(d[4], d[5])
  }
}

module.exports = {
  getPushUri: getPushUri,
  getRedisUri: getRedisUri,
  getRedisIP: getRedisIP,
  getRedisPORT: getRedisPORT,
  parse: parse,
  getRedisPassword: getRedisPassword
}
