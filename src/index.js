'use strict'

const server = require('./server')
const winston = require('winston')

const logger = winston.createLogger({
  transports: [new winston.transports.Console({ timestamp: true })]
})

server.on('ready', () => {
  logger.log({
    level: 'info',
    message: 'TCP server ready'
  })
})

server.on('info', message => {
  logger.log({
    level: 'info',
    message: message
  })
})

server.on('error', err => {
  logger.log({
    level: 'error',
    message: err
  })
  process.exit(1)
})

server.on('close', err => {
  logger.log({
    level: 'error',
    message: `Server closes: ${err}`
  })
  process.exit(1)
})

process.on('uncaughtException', err => {
  logger.log({
    level: 'error',
    message: `uncaughtException: ${err}`
  })
  process.exit(1)
})
