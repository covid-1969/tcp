'use strict'

const tcp = require('./tcp')
const zmq = require('./zmq')
const utils = require('./utils')
const Redis = require('ioredis')
const EventEmitter = require('events').EventEmitter

const event = new EventEmitter()

/** *** REDIS ******/
const client = new Redis(utils.getRedisUri())

client.on('error', err => event.emit('error', `Error with Redis -> ${err}`))
client.on('connect', () => {
  event.emit('info', `Connected to Redis in ${utils.getRedisIP()}`)

  /** *** ZEOMQ *****/
  zmq.push.connect(utils.getPushUri())
})

tcp.ready.subscribe(
  () => event.emit('ready', true),
  err => event.emit('error', err)
)
tcp.error.subscribe(err => event.emit('error', `Error in TCP -> ${err}`))

/** **** CLIENTS *******/
// New connection
tcp.connection.subscribe(
  socket => {
    event.emit('info', `New client connected: ${socket.id}`)
    // sockets[socket.id] = socket
  },
  err => event.emit('error', `Error with TCP connection -> ${err}`)
)

// New data receibed from client
const data = tcp.data.filter(data => data.event === 'data')
data.subscribe(
  data => {
    let _data = utils.parse(data.raw.toString())
    if (_data) {
      // Saves data to Redis
      const values = [
        'userType',
        _data.userType,
        'datetime',
        _data.datetime,
        'lat',
        _data.point.coordinates[1],
        'lng',
        _data.point.coordinates[0],
        'geohash',
        _data.geohash
      ]
      client.hmset(`users:${_data.userId}`, values).then((res, err) => {
        if (err) {
          event.emit('info', `Error setting data in Redis: ${err}`)
        } else {
          event.emit(
            'info',
            `New data received: ${_data.userId}->${_data.datetime}`
          )
          // Send data to checker
          const message = ['data', JSON.stringify(_data)]
          zmq.push.send(message)
        }
      })
    } else {
      event.emit(
        'info',
        `Data with bad format received: ${data.raw.toString()}`
      )
    }
  },
  err => event.emit('error', `Error with TCP data -> ${err}`)
)

// Client closed the connection
const close = tcp.data.filter(data => data.event === 'close')
close.subscribe(
  data => {
    // delete sockets[data.socket.id]
    event.emit('info', `Client ${data.socket.id} disconnected`)
    data.socket.destroy()
  },
  err => event.emit('error', `Error with TCP close -> ${err}`)
)

// Connection Timeout with client
const timeout = tcp.data.filter(data => data.event === 'timeout')
timeout.subscribe(
  data => {
    // delete sockets[data.socket.id]
    event.emit('info', `Client ${data.socket.id} disconnected by timeout`)
    data.socket.end()
  },
  err => event.emit('error', `Error with TCP timeout -> ${err}`)
)

// Error with client
const error = tcp.data.filter(data => data.event === 'error')
error.subscribe(
  data => {
    // delete sockets[data.socket.id]
    event.emit('info', `Client ${data.socket.id} disconnected for an error`)
    data.socket.destroy()
  },
  err => event.emit('error', `Error with TCP error -> ${err}`)
)

module.exports = event
