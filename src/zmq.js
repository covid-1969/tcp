'use strict'

const Rx = require('rx')
const zmq = require('zeromq/v5-compat')
const utils = require('./utils')

const socketPush = zmq.socket('push')

// connection
socketPush.connect(utils.getPushUri())
const socketPushConnection = Rx.Observable.fromEvent(socketPush, 'connection')

module.exports = {
  connection: socketPushConnection,
  push: socketPush
}
