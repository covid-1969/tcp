'use strict'

const net = require('net')
const Rx = require('rx')
const { v4: uuidv4 } = require('uuid')

let port
if (process.env.PORT) {
  port = parseInt(process.env.PORT, 10)
} else {
  port = 19969
}
// 2 hours default timeout for no signals.
const TIMEOUT = parseInt(process.env.TIMEOUT || 7200000, 10)
const server = net.createServer().listen(port)

// server ready
const ready = Rx.Observable.fromEvent(server, 'listening')

// server error
const error = Rx.Observable.fromEvent(server, 'error')

// new connection
const source = Rx.Observable.fromEvent(server, 'connection').takeUntil(
  Rx.Observable.fromEvent(server, 'close')
)

// sets socket id and timeout on new connection
const connection = source.map(socket => {
  socket.id = uuidv4()
  socket.setTimeout(TIMEOUT)
  return socket
})

const data = source.flatMap(socket => {
  // new data
  const data = Rx.Observable.fromEvent(socket, 'data').map(raw => ({
    event: 'data',
    raw: raw,
    socket: socket
  }))
  // close connection
  const close = Rx.Observable.fromEvent(socket, 'close').map(() => ({
    event: 'close',
    socket: socket
  }))
  // timeout connection
  const timeout = Rx.Observable.fromEvent(socket, 'timeout').map(() => ({
    event: 'timeout',
    socket: socket
  }))
  // error
  const error = Rx.Observable.fromEvent(socket, 'error').map(err => ({
    event: 'error',
    socket: socket,
    error: err
  }))

  return data
    .merge(close)
    .merge(timeout)
    .merge(error)
})

module.exports = {
  ready: ready,
  error: error,
  connection: connection,
  data: data
}
